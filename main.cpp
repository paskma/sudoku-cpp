#include <iostream>
#include <cassert>
#include <chrono>

#include "board.h"


using namespace std;

int puzzle[] = {
 0,0,2,5,0,0,0,0,3,
 0,4,0,0,6,7,0,0,0,
 1,5,0,0,0,3,0,0,0,
 0,0,8,0,0,0,0,0,4,
 5,6,0,0,0,0,0,1,7,
 4,0,0,0,0,0,8,0,0,
 0,0,0,6,0,0,0,8,1,
 0,0,0,1,8,0,0,2,0,
 2,0,0,0,0,5,7,0,0
};

int puzzle_empty[] = {
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
};

const int _ = 0;

int puzzle_hardest[] = {
 8,_,_, _,_,_, _,_,_,
 _,_,3, 6,_,_, _,_,_,
 _,7,_, _,9,_, 2,_,_,

 _,5,_, _,_,7, _,_,_,
 _,_,_, _,4,5, 7,_,_,
 _,_,_, 1,_,_, _,3,_,

 _,_,1, _,_,_, _,6,8,
 _,_,8, 5,_,_, _,1,_,
 _,9,_, _,_,_, 4,_,_,
};

int puzzle_solved[] = {
7,9,2,5,1,8,6,4,3,
8,4,3,9,6,7,1,5,2,
1,5,6,4,2,3,9,7,8,
3,2,8,7,9,1,5,6,4,
5,6,9,8,3,4,2,1,7,
4,1,7,2,5,6,8,3,9,
9,3,5,6,7,2,4,8,1,
6,7,4,1,8,9,3,2,5,
2,8,1,3,4,5,7,9,6
};

// http://norvig.com/sudoku.html
int puzzle_norvig[] = {
    _, _, _,  _, _, 6,  _, _, _,
    _, 5, 9,  _, _, _,  _, _, 8,
    2, _, _,  _, _, 8,  _, _, _,

    _, 4, 5,  _, _, _,  _, _, _,
    _, _, 3,  _, _, _,  _, _, _,
    _, _, 6,  _, _, 3,  _, 5, 4,

    _, _, _,  3, 2, 5,  _, _, 6,
    _, _, _,  _, _, _,  _, _, _,
    _, _, _,  _, _, _,  _, _, _,
};

int main()
{
    Board board(puzzle_hardest);
    board.print();
    cout << "Solving..." << endl;
    auto start = std::chrono::system_clock::now();
    bool suc = board.solve();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::system_clock::now() - start);
    auto msduration = ((double)duration.count()) / 1000;
    cout << "..." << suc << " t=" << msduration << "ms" << endl;

    board.print();

    cout << "Done." << endl;
    return 0;
}
