#include "board.h"

#include <sstream>
#include <iostream>
#include <algorithm>

Board::Board(int* data) : m_data(data)
{

}

inline int& Board::get(int row, int col)
{
    return m_data[row * 9 + col];
}

std::string Board::toString()
{
    std::stringstream result;
    for (int row = 0; row < 9; row++) {
        for (int col = 0; col < 9; col++) {
            if (get(row, col) != 0) {
                result << get(row, col);
            } else {
                result << " ";
            }
            if ((col+1) % 3 == 0 && col != 8)
                result << "|";
        }
        result << std::endl;
        if ((row + 1) % 3 == 0) {
            result << "---+---+---" << std::endl;
        }
    }
    return result.str();
}

bool Board::fitsInRow(int n, int row)
{
    for (int k = 0; k < 9; k++) {
        if (get(row, k) == n)
            return false;
    }

    return true;
}

bool Board::fitsInCol(int n, int col)
{
    for (int k = 0; k < 9; k++) {
        if (get(k, col) == n)
            return false;
    }

    return true;
}

bool Board::fitsInBlock(int n, int row, int col)
{
    for (int k = 0; k < 3; k++) {
        for (int l = 0; l < 3; l++) {
            if (get(k + row - row%3, l + col - col%3) == n)
                return false;
        }
    }

    return true;
}

bool Board::fits(int n, int row, int col)
{
    return fitsInRow(n, row)
            && fitsInCol(n, col)
            && fitsInBlock(n, row, col);

}

std::vector<int> Board::possible(int row, int col)
{
    std::vector<int> result;

    if (get(row, col) != 0)
        return result;

    result.reserve(9);

    for (int i = 1; i <= 9; i++) {
        if (fits(i, row, col))
            result.push_back(i);
    }


    return result;
}

struct Possibility
{
    int row, col;
    std::vector<int> numbers;

    Possibility(const Possibility&) = default;
    Possibility(Possibility&&) = default;
    Possibility& operator=(const Possibility&) = default;
    Possibility& operator=(Possibility&&) = default;
    Possibility(int row, int col, std::vector<int> numbers) :
        row(row), col(col), numbers(numbers) {}

    bool operator<(const Possibility& p) const {
        return numbers.size() < p.numbers.size();
    }
};

bool Board::solve()
{
    return solveImpl(0);
}

bool Board::solveImpl(int level)
{
    std::vector<Possibility> ps;
    ps.reserve(81);

    int zeroes(0);
    for (int row = 0; row < 9; row++) {
        for (int col = 0; col < 9; col++) {
            if (get(row, col) == 0) {
                zeroes++;
            } else {
                continue;
            }

            auto p = possible(row, col);
            if (p.size() > 0) {
                ps.push_back(Possibility(row, col, p));
            } else {
                return false;
            }
        }
    }

    if (zeroes == 0)
        return true;

    if (ps.size() == 0)
        return false;

    Possibility& current(*std::min_element(ps.begin(), ps.end()));

    for (auto i = current.numbers.begin(); i != current.numbers.end(); ++i) {
        get(current.row, current.col) = *i;

        //std::cout << "On level " << level << " pos " << current.row << ", " << current.col << " trying "
        //          << *i << " total " << current.numbers.size() << std::endl;

        if (solveImpl(level + 1))
            return true;
    }

    get(current.row, current.col) = 0;
    return false;
}

void Board::print() {
    std::cout << toString();
}
