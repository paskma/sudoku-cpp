#ifndef BOARD_H
#define BOARD_H

#include <string>
#include <vector>

class Board
{
public:
    Board(int* data);
    inline int &get(int row, int col);
    std::string toString();
    void print();
    bool fits(int n, int row, int col);
    bool solve();
public:
    int* m_data;

    bool solveImpl(int level);
    bool fitsInBlock(int n, int row, int col);
    bool fitsInCol(int n, int col);
    bool fitsInRow(int n, int row);
    std::vector<int> possible(int row, int col);

};

#endif // BOARD_H
